package services

import (
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"

	"gitlab.com/EMP/api-gateway/config"
	us "gitlab.com/EMP/api-gateway/genproto/user"
)


type IServiceManager interface {
	UserService() us.UserServiceClient
}

type serviceManager struct {
	userService  us.UserServiceClient
}

func (s *serviceManager) UserService() us.UserServiceClient {
	return s.userService
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	resolver.SetDefaultScheme("dns")

	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.UserServiceHost, conf.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	serviceManager := &serviceManager{
		userService: us.NewUserServiceClient(connUser),
	}

	return serviceManager, nil
}

