package models

import (
	user "gitlab.com/EMP/api-gateway/genproto/user"
)

type UserRequest struct {
	ProfileImage string             `json:"profile_image"`
	PhoneNumber  string             `json:"phone_number"`
	FirstName    string             `json:"first_name"`
	LastName     string             `json:"last_name"`
	Username     string             `json:"user_name"`
	Email        string             `json:"email"`
	Password     string             `json:"password"`
	Addresses    []*user.AddressReq `json:"addresses"`
}

type UserVerification struct {
	ProfileImage string              `json:"profile_image"`
	PhoneNumber  string              `json:"phone_number"`
	FirstName    string              `json:"first_name"`
	LastName     string              `json:"last_name"`
	Username     string              `json:"user_name"`
	Email        string              `json:"email"`
	Password     string              `json:"password"`
	Addresses    []*user.AddressResp `json:"addresses"`
	RefreshToken string              `json:"refreshtoken"`
	AccessToken  string              `json:"accessToken"`
}

type AddressReq struct {
	Country string `json:"country"`
	City    string `json:"city"`
	Town    string `json:"town"`
	HomeNum string `json:"home_num"`
}

type UserRedis struct {
	ProfileImage string             `json:"profile_image"`
	PhoneNumber  string             `json:"phone_number"`
	FirstName    string             `json:"first_name"`
	LastName     string             `json:"last_name"`
	Username     string             `json:"user_name"`
	Email        string             `json:"email"`
	User_type    string             `json:"user_type"`
	Password     string             `json:"password"`
	Code         string             `json:"code"`
	Addresses    []*user.AddressReq `json:"addresses"`
}
