package models

type FailureInfo struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Error   error  `json:"error"`
}

type SuccessInfo struct {
	Message    string `json:"message"`
	StatusCode int    `json:"status_code"`
}
