package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.com/EMP/api-gateway/api/models"
	user "gitlab.com/EMP/api-gateway/genproto/user"
	l "gitlab.com/EMP/api-gateway/pkg/logger"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"google.golang.org/protobuf/encoding/protojson"
)

// Verify customer
// @Summary      Verify user
// @Description  Verifys user
// @Tags         User
// @Security BearerAuth
// @Accept       json
// @Produce      json
// @Param        email  path string true "email"
// @Param        code   path string true "code"
// @Success      200  {object}  models.UserVerification
// @Router      /v1/user/verify/{email}/{code} [get]
func (h *handlerV1) Verifacition(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		code  = c.Param("code")
		email = c.Param("email")
	)

	sredis, err := h.redis.Get(email)
	fmt.Println(email, code)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{
			"info":  "Your time has expired",
			"error": err.Error(),
		})
		h.log.Error("Error while getting user from redis", l.Any("redis", err))
		return
	}
	cs := cast.ToString(sredis)
	csStr := models.UserRedis{}
	err = json.Unmarshal([]byte(cs), &csStr)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while unmarshaling from json to user csStr", l.Any("json", err))
		return
	}

	if csStr.Code != code {
		fmt.Println(csStr.Code)
		c.JSON(http.StatusConflict, gin.H{
			"info": "Wrong code",
		})
		return
	}

	user_id := uuid.New().String()

	body := user.UserReq{
		UserId:       user_id,
		ProfileImage: csStr.ProfileImage,
		PhoneNumber:  csStr.PhoneNumber,
		FirstName:    csStr.FirstName,
		LastName:     csStr.LastName,
		UserName:     csStr.Username,
		Email:        csStr.Email,
		Password:     csStr.Password,
	}
	h.jwthandler.Iss = "user"
	h.jwthandler.Sub = body.UserId
	h.jwthandler.Role = "authorized"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignKey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessToken := tokens[0]
	refreshToken := tokens[1]
	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}

	body.RefreshToken = refreshToken

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	id := uuid.New().String()
	body.UserId = string(id)
	fmt.Println(body)
	res, err := h.serviceManager.UserService().CreateUser(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating customer", l.Any("post", err))
		return
	}

	response := models.UserVerification{
		ProfileImage: res.ProfileImage,
		PhoneNumber:  res.PhoneNumber,
		FirstName:    res.FirstName,
		LastName:     res.LastName,
		Username:     res.UserName,
		Email:        res.Email,
		Password:     res.Password,
		Addresses: res.Addre,
	}

	response.AccessToken = accessToken
	response.RefreshToken = refreshToken

	c.JSON(http.StatusOK, response)

}
