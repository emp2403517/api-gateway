package v1

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"
	l "gitlab.com/EMP/api-gateway/pkg/logger"
	"gitlab.com/EMP/api-gateway/genproto/user"
	"gitlab.com/EMP/api-gateway/api/models"
	"github.com/gin-gonic/gin"
	"gitlab.com/EMP/api-gateway/pkg/otlp"
	"google.golang.org/protobuf/encoding/protojson"
)

// Get User By Id
// @Summary      GetUserById
// @Description  Getting user info and addresses
// @Tags         User
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id 	path 		string   true  "user id"
// @Success      200  	{object}  	user.UserResp
// @Router       /v1/user/get/{id} [get]
func (h *handlerV1) GetUserByID(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	uuid := c.Param("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetUserByID")
	defer span.End()
	response, err := h.serviceManager.UserService().GetUserByID(ctx, &user.ID{ID: uuid})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get users")
		return
	}
	c.JSON(http.StatusOK, response)

}

// Get User By Id
// @Summary      GetUserById
// @Description  Getting user info and addresses
// @Tags         Sudo
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id 	path 		string   true  "user id"
// @Success      200  	{object}  	user.Empty
// @Router       /v1/sudo/create_admin/{id} [patch]
func (h *handlerV1) CreateAdmin(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	uuid := c.Param("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "CreateAdmin")
	defer span.End()
	response, err := h.serviceManager.UserService().CreateAdmin(ctx, &user.ID{ID: uuid})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get users")
		return
	}
	c.JSON(http.StatusOK, response)
}

// Delete User By Id
// @Summary      DeleteUserById
// @Description  Delete user info and addresses
// @Tags         Sudo
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id 	path 		string   true  "user id"
// @Success      200  	{object}  	user.Empty
// @Router       /v1/user/delete/{id} [delete]
func (h *handlerV1) DeleteUser(c *gin.Context) {
	uuid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "DeleteUser")
	defer span.End()
	response, err := h.serviceManager.UserService().DeleteUser(ctx, &user.ID{ID: uuid})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get users")
		return
	}
	c.JSON(http.StatusOK, response)

}

// Get User
// @Summary      Get user
// @Description  Get user info and addresses
// @Tags         User
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        page 	path 		int64   true  "page"
// @Param        limit 	path 		int64   true  "limit"
// @Param        keyboard 	path 		string   true  "keyboard"
// @Success      200  	{object}  	user.Users
// @Router       /v1/user/get/all/{page}/{limit}/{keyboard} [get]
func (h *handlerV1) GetUserAll(c *gin.Context) {
	page := c.Param("page")
	limit := c.Param("limit")
	keyboard := c.Param("keyboard")
	page1, err := strconv.Atoi(page)
	if err != nil {
		fmt.Println(err)
	}
	limit1, err := strconv.Atoi(limit)
	if err != nil {
		fmt.Println(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetUserAll")
	defer span.End()
	response, err := h.serviceManager.UserService().GetUserAll(ctx, &user.AllUserParamsRequest{Page: int64(page1),
		Limit:    int64(limit1),
		Keyboard: keyboard})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get users")
		return
	}	

	c.JSON(http.StatusOK, response)
}

// Get User Address By Id
// @Summary      GetUserAddressById
// @Description  Get user addresses
// @Tags         user
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        user_id 	path 		string   true  "user id"
// @Success      200  	{object}  	user.Adress
// @Router       /v1/user/get/address/{user_id} [get]
func (h *handlerV1) GetAddressByUserID(c *gin.Context) {
	uuid := c.Param("user_id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetAddressByUserID")
	defer span.End()
	response, err := h.serviceManager.UserService().GetAddressByUserID(ctx, &user.ID{ID: uuid})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get users")
		return
	}
	c.JSON(http.StatusOK, response)

}
// Get User By Country
// @Summary      Get user 
// @Description  Get User By Country
// @Tags         User
// @Security BearerAuth
// @Accept       json
// @Produce      json
// @Param        country   body 	  user.AdrSu  true  "Country"
// @Success      200  	{object}  user.Users
// @Router       /v1/user/get/country [get]
func (h *handlerV1) GetUserByCountry(c *gin.Context) {
	body := user.AdrSu{}

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("error while bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetUserByCountry")
	defer span.End()
	response, err :=h.serviceManager.UserService().GetUserByCountry(ctx, &user.AdrSu{Somtinhg: body.Somtinhg})
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("failed get user", l.Error(err))
		return
	}
	
	c.JSON(http.StatusOK, response)
}

// Get User By Country
// @Summary      Get user 
// @Description  Get User By Country
// @Tags         User
// @Security BearerAuth
// @Accept       json
// @Produce      json
// @Param        City   body 	  user.AdrSu  true  "City"
// @Success      200  	{object}  user.Users
// @Router       /v1/user/get/city [get]
func (h *handlerV1) GetUserByCity(c *gin.Context) {
	body := user.AdrSu{}

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("error while bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetUserByCity")
	defer span.End()
	response, err :=h.serviceManager.UserService().GetUserByCity(ctx, &user.AdrSu{Somtinhg: body.Somtinhg})
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("failed get user", l.Error(err))
		return
	}
	
	c.JSON(http.StatusOK, response)
}

// Get User By Country
// @Summary      Get user 
// @Description  Get User By Country
// @Tags         User
// @Security BearerAuth
// @Accept       json
// @Produce      json
// @Param        town   body 	  user.AdrSu  true  "Town"
// @Success      200  	{object}  user.Users
// @Router       /v1/user/get/town [get]
func (h *handlerV1) GetUserByTown(c *gin.Context) {
	body := user.AdrSu{}

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("error while bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetUserByTown")
	defer span.End()
	response, err :=h.serviceManager.UserService().GetUserByTown(ctx, &user.AdrSu{Somtinhg: body.Somtinhg})
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("failed get user", l.Error(err))
		return
	}
	
	c.JSON(http.StatusOK, response)
}


// User Update
// @Summary      User update
// @Description  User update
// @Tags         Sudo
// @Security BearerAuth
// @Accept       json
// @Produce      json
// @Param        user   body 	  user.UpdateUserReq  true  "Update"
// @Success      200  	{object}  user.UserResp
// @Router       /v1/user/update [patch]
func (h * handlerV1) UpdateUser(c *gin.Context) {
	body := user.UpdateUserReq{}

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("error while bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "UpdateUser")
	defer span.End()
	response, err :=h.serviceManager.UserService().UpdateUser(ctx, &body)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("failed get user", l.Error(err))
		return
	}
	
	c.JSON(http.StatusOK, response)
}



// Get User Address By Id
// @Summary      GetUserAddressById
// @Description  Get user addresses
// @Tags         user
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        address_id 	path 		string   true  "address id"
// @Success      200  	{object}  	user.Adress
// @Router       /v1/user/get/addressID/{address_id} [get]
func (h *handlerV1) GetUserByAdress(c *gin.Context) {
	uuid := c.Param("address_id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetUserByAdress")
	defer span.End()
	response, err := h.serviceManager.UserService().GetUserByAdress(ctx, &user.ID{ID: uuid})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get users")
		return
	}
	c.JSON(http.StatusOK, response)
}
