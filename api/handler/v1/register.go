package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/EMP/api-gateway/api/models"
	"gitlab.com/EMP/api-gateway/genproto/user"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/EMP/api-gateway/pkg/etc"
	l "gitlab.com/EMP/api-gateway/pkg/logger"

	//"golang.org/x/crypto/bcrypt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	email "gitlab.com/EMP/api-gateway/pkg/email"
)

// Register
// @Summary      Register
// @Description  Registration
// @Tags         User
// @Security BearerAuth
// @Accept       json
// @Produce      json
// @Param        user   body 	  models.UserRequest  true  "Users"
// @Success      200  	{object}  user.UserResp
// @Router       /v1/user/register [post]
func (h *handlerV1) RegisterUser(c *gin.Context) {

	body := models.UserRequest{}

	err := c.ShouldBindJSON(&body)
	fmt.Println(body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("error while bind json", l.Error(err))
		return
	}

	err = email.IsValidMail(body.Email)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		return
	}
	body.Email = strings.ToLower(body.Email)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	fmt.Println(body.Email)
	existsEmail, err := h.serviceManager.UserService().CheckField(ctx, &user.CheckFieldReq{
		Field: "email",
		Value: body.Email,
	})
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("failed check email uniques", l.Error(err))
		return
	}
	if existsEmail.Exists {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "please enter another email",
			Error:   err,
		})
		h.log.Error("this email already exists", l.Error(err))
		return
	}

	exists, err := h.redis.Exists(body.Email)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "please erter another email",
			Error:   err,
		})
		h.log.Error("failed check email uniques", l.Error(err))
		return
	}

	if cast.ToInt(exists) == 1 {
		c.JSON(http.StatusConflict, gin.H{
			"error": err.Error(),
		})
		return
	}

	hashPass, err := bcrypt.GenerateFromPassword([]byte(body.Password), 10)

	if err != nil {
		h.log.Error("error while hashing password", l.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong",
		})
		return
	}
	code := etc.GenerateCode(6)
	body.Password = string(hashPass)

	ref := &models.UserRedis{
		ProfileImage: body.ProfileImage,
		PhoneNumber:  body.PhoneNumber,
		FirstName:    body.FirstName,
		LastName:     body.LastName,
		Email:        body.Email,
		User_type:    "user",
		Username:     body.Username,
		Password:     body.Password,
		Addresses:    body.Addresses,
		Code:         code,
	}
	msg := "Subject: Exam email verification\n Your verification code: " + ref.Code
	err = email.SendEmail([]string{ref.Email}, []byte(msg))

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.FailureInfo{
			Error:   err,
			Code:    http.StatusInternalServerError,
			Message: "Email unavailable",
		})
		return
	}

	jsCustomer, err := json.Marshal(ref)

	if err != nil {
		h.log.Error("error while marshaling user,inorder to insert it to redis", l.Error(err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "error while creating user",
		})
		return
	}

	if err = h.redis.SetWithTTL(string(ref.Email), string(jsCustomer), 300); err != nil {
		fmt.Println(err)
		h.log.Error("error while inserting new user into redis")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong, please try again",
		})
		return
	}
}
