package v1

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/EMP/api-gateway/api/models"
	"gitlab.com/EMP/api-gateway/genproto/user"
	"gitlab.com/EMP/api-gateway/pkg/etc"
	"gitlab.com/EMP/api-gateway/pkg/logger"
	"gitlab.com/EMP/api-gateway/pkg/otlp"
	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Login user
// @Summary      Login user
// @Description  Logins user
// @Tags         User
// @Security BearerAuth
// @Accept       json
// @Produce      json
// @Param        email  	path string true "email"
// @Param        password   path string true "password"
// @Success      200     	{object}  models.UserVerification
// @Router      /v1/user/login/{email}/{password} [get]
func (h *handlerV1) LoginUser(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		password = c.Param("password")
		email    = c.Param("email")
	)
	fmt.Println(password, email)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "LoginUser")
	defer span.End()
	res, err := h.serviceManager.UserService().GetUserByEmail(ctx, &user.Email{
		Email: email,
	})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "Couln't find matching information, Have you registered before?",
		})
		h.log.Error("Error while getting customer by email", logger.Any("post", err))
		return
	}

	if !etc.CheckPasswordHash(password, res.Password) {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "Password or Email error",
		})
		return
	}

	h.jwthandler.Iss = "user"
	h.jwthandler.Sub = res.Id
	if res.UserType == "sudo"{
		h.jwthandler.Role = "sudo"
	}else if res.UserType == "admin"{
		h.jwthandler.Role = "admin"
	} else {
		h.jwthandler.Role = "unauthorized"
	}
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignKey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessesToken := tokens[0]
	refreshToken := tokens[1]

	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	response := models.UserVerification{
		ProfileImage: res.ProfileImage,
		PhoneNumber:  res.PhoneNumber,
		FirstName:    res.FirstName,
		LastName:     res.LastName,
		Username:     res.UserName,
		Email:        res.Email,
		Password:     res.Password,
		Addresses: res.Addre,
	}

	response.AccessToken = accessesToken
	response.RefreshToken = refreshToken

	response.Password = ""
	c.JSON(http.StatusOK, response)
}
