package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment string // develop, staging, production
	//custumer
	App string
	UserServiceHost string
	UserServicePort int
	CtxTimeout int

	RedisHost string
	RedisPort string

	Rouls string
	SignKey string
	AuthConfigPath string
	LogLevel string
	OTLPCollectorHost string
	OTLPCollectorPort string
	HTTPPort string
}

// Load loads environment vars and inflates Config
func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))
	c.App = cast.ToString(getOrReturnDefault("APP", "api"))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":9099"))

	c.UserServiceHost = cast.ToString(getOrReturnDefault("CUSTUMER_SERVICE_HOST", "localhost"))
	c.UserServicePort = cast.ToInt(getOrReturnDefault("CUSTUMER_SERVICE_PORT", 5557))

	c.RedisHost = cast.ToString(getOrReturnDefault("REDIS_HOST","localhost"))
	c.RedisPort = cast.ToString(getOrReturnDefault("REDIS_PORT","6379"))
	c.OTLPCollectorHost = cast.ToString(getOrReturnDefault("OTLP_COLLECTOR_HOST", "localhost"))
	c.OTLPCollectorPort = cast.ToString(getOrReturnDefault("OTLP_COLLECTOR_PORT", ":4317"))
	c.SignKey = cast.ToString(getOrReturnDefault("SIGN_KEY", "secret"))
	c.AuthConfigPath = cast.ToString(getOrReturnDefault("AUTH_PATH", "./config/auth.conf"))
	c.CtxTimeout = cast.ToInt(getOrReturnDefault("CTX_TIMEOUT", 7))



	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
