package v1

import (
	"github.com/casbin/casbin/v2"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	jwthandler "gitlab.com/EMP/api-gateway/api/tokens"
	t "gitlab.com/EMP/api-gateway/api/tokens"
	"gitlab.com/EMP/api-gateway/config"
	"gitlab.com/EMP/api-gateway/pkg/logger"
	"gitlab.com/EMP/api-gateway/service"
	"gitlab.com/EMP/api-gateway/storage/repo"
	"go.opentelemetry.io/otel/trace"
)

type handlerV1 struct {
	log            logger.Logger
	serviceManager services.IServiceManager
	Tracer trace.Tracer
	cfg            config.Config
	redis          repo.InMemoryStorageI
	jwthandler     t.JWTHandler
	CasbinEnforcer casbin.Enforcer
}

// handlerV1Config ...
type HandlerV1Config struct {
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Tracer trace.Tracer
	Cfg            config.Config
	Redis          repo.InMemoryStorageI
	JWTHandler     t.JWTHandler
	Casbin casbin.Enforcer
	

}

// New ...

func New(c *HandlerV1Config) *handlerV1 {
	return &handlerV1{
		log:            c.Logger,
		serviceManager: c.ServiceManager,
		cfg:            c.Cfg,
		redis:          c.Redis,
		jwthandler:     c.JWTHandler,
		CasbinEnforcer: c.Casbin,
	}

}

func GetClaims(h handlerV1, c *gin.Context) (*jwthandler.CustomClaims, error) {

	var (
		claims = jwthandler.CustomClaims{}
	)
	
	strToken := c.GetHeader("Authorization")
	
	token, err := jwt.Parse(strToken, func(t *jwt.Token) (interface{}, error) {return []byte(h.cfg.SignKey), nil})
	
	if err != nil {
		//fmt.Println(err)
		h.log.Error("invalid access token")
		return nil, err
	}
	// rawClaims := token.Claims.(jwt.MapClaims)

	// claims.Sub = rawClaims["sub"].(string)
	// claims.Exp = rawClaims["exp"].(float64)
	// fmt.Printf("%T type of value in map %v",rawClaims["exp"],rawClaims["exp"])
	// fmt.Printf("%T type of value in map %v",rawClaims["iat"],rawClaims["iat"])

	claims.Token = token
	return &claims, nil

//	}
//func GetClaims(h *handlerV1, c *gin.Context) jwt.MapClaims {
//	var (
//		authorization models.GetPageOfUsersRequest
//	)

}
