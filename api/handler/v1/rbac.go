package v1

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/EMP/api-gateway/api/models"
	users "gitlab.com/EMP/api-gateway/genproto/user"
)


// @Summary      Add Policy User
// @Description  Add policy for user
// @Tags         Sudo
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        policy   body 	  models.Policy  true  "Policy"
// @Success      200  	{object}  	user.Empty
// @Router      /v1/admin/add/policy [POST]
func (h *handlerV1) AddPolicy(c *gin.Context) {
	
	body := models.Policy{}

	err := c.ShouldBindJSON(&body)
	if err != nil {
		fmt.Println(err)
	}
	ok, err := h.CasbinEnforcer.AddPolicy(body.User, body.Domain, body.Action)
	if err != nil {
		fmt.Println(">>>>>",err)
	}
	h.CasbinEnforcer.SavePolicy()
	fmt.Println(ok)
	c.JSON(http.StatusOK, users.Empty{})
}

// @Summary      Remove Policy User
// @Description  Remove user Policy
// @Tags         Sudo
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        policy   body 	  models.Policy  true  "Policy"
// @Success      200  	{object}  	user.Empty
// @Router      /v1/admin/remove/policy [POST]
func (h *handlerV1) RemovePolicy(c *gin.Context) {
	body := models.Policy{}

	err := c.ShouldBindJSON(&body)
	if err != nil {
		fmt.Println(err)
	}
	ok, err := h.CasbinEnforcer.RemovePolicy(body.User, body.Domain, body.Action)
	if err != nil {
		fmt.Println(err)
	}
	h.CasbinEnforcer.SavePolicy()
	fmt.Println(ok)
	c.JSON(http.StatusOK, users.Empty{})
}