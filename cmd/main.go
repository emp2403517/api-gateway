package main

import (

	"github.com/casbin/casbin/v2"
	defaultrolemanager "github.com/casbin/casbin/v2/rbac/default-role-manager"
	"github.com/casbin/casbin/v2/util"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/EMP/api-gateway/api"
	"gitlab.com/EMP/api-gateway/pkg/otlp"
	"gitlab.com/EMP/api-gateway/config"
	"gitlab.com/EMP/api-gateway/pkg/logger"
	"gitlab.com/EMP/api-gateway/service"
	r "gitlab.com/EMP/api-gateway/storage/redis"

)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "api_gateway")

	serviceManager, err := services.NewServiceManager(&cfg)
	if err != nil {
		log.Error("gRPC dial error", logger.Error(err))
	}
	shutdownOTLP, err := otlp.InitOTLPProvider(&cfg)
	if err != nil {
		log.Error("Init OTPL")
	}
	defer shutdownOTLP()
	casbinEnforcer, err := casbin.NewEnforcer(cfg.AuthConfigPath, "./config/auth.csv")	
	if err != nil {
		log.Error("Casbin conn")
	}
	err = casbinEnforcer.LoadPolicy()
	if err != nil {
		log.Error("casbin error load policy", logger.Error(err))
		return
	}
	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch", util.KeyMatch)
	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch3", util.KeyMatch3)
	pool := &redis.Pool{
		MaxIdle: 10,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", cfg.RedisHost+":"+cfg.RedisPort)
		},
	}

	server := api.New(api.Option{
		Conf:           cfg,
		Logger:         log,
		ServiceManager: serviceManager,
		ShutdownOTLP:   shutdownOTLP,
		Redis:          r.NewRedisRepo(pool),
		CasbinEnforcer: casbinEnforcer,
	})

	if err := server.Run(cfg.HTTPPort); err != nil {
		log.Fatal("failed to run http server", logger.Error(err))
		panic(err)
	}
}
