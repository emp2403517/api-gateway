package api

import (
	"gitlab.com/EMP/api-gateway/config"
	"gitlab.com/EMP/api-gateway/pkg/logger"
	"gitlab.com/EMP/api-gateway/service"
	"gitlab.com/EMP/api-gateway/api/middleware"
	"gitlab.com/EMP/api-gateway/storage/repo"
	"github.com/gin-contrib/cors"
	_ "gitlab.com/EMP/api-gateway/api/docs" //swag
	v1 "gitlab.com/EMP/api-gateway/api/handler/v1"
	jwthandler "gitlab.com/EMP/api-gateway/api/tokens"
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)
// Option ...
type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ShutdownOTLP   func() error
	ServiceManager services.IServiceManager
	Redis          repo.InMemoryStorageI
	CasbinEnforcer *casbin.Enforcer
}

// New ...
// @title           EMP.uz api
// @version         1.0
// @description     This is EMP.uz server api server
// @termsOfService  2 term EMP.uz
// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
// @contact.name   Abduazim
// @contact.url    https://t.me/Abduazim_Kabulov
// @contact.email  kabulovabduazim@gmail.com
// @BasePath  /v1
func New(option Option) *gin.Engine {
	router := gin.New()
	corConfig := cors.DefaultConfig()
  	corConfig.AllowAllOrigins = true
 	corConfig.AllowCredentials = true
 	corConfig.AllowHeaders = []string{"*"}
  	corConfig.AllowBrowserExtensions = true
  	corConfig.AllowMethods = []string{"*"}
	router.Use(gin.Logger())
	router.Use(gin.Recovery())


	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
		Redis:          option.Redis,
		Casbin: *option.CasbinEnforcer,
	})

	jwt := jwthandler.JWTHandler{
		SigninKey: option.Conf.SignKey,
		Log:       option.Logger,
	}
	
	router.Use(middleware.NewAuth(option.CasbinEnforcer, jwt, config.Load()))

	api := router.Group("/v1")
	api.POST("/user/register", handlerV1.RegisterUser)
	api.GET("/user/login/:email/:password", handlerV1.LoginUser)
	api.GET("/user/verify/:email/:code", handlerV1.Verifacition)
	api.POST("/sudo/register", handlerV1.RegisterSudo)
	api.POST("/admin/add/policy", handlerV1.AddPolicy)
	api.POST("/admin/remove/policy", handlerV1.RemovePolicy)
	api.GET("/user/get/:id", handlerV1.GetUserByID)
	api.PATCH("/sudo/create_admin/:id", handlerV1.CreateAdmin)
	api.GET("/sudo/verify/:email/:code", handlerV1.VerifacitionSudo)
	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler, url))

	return router

}
